package com.mobilnylistonosz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ACTION_NAME = "action_name";
    public static final int ALL_CODE = 1;
    public static final int DELIVER_CODE = 2;
    public static final int OTHER_CODE = 3;
    public static final int NOTIFY_CODE = 4;

    private LetterViewModel letterViewModel;

    private DrawerLayout drawerLayout;

    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_layout);

        LinearLayout allLettersButton = findViewById(R.id.all_letters_button);
        LinearLayout deliveredLettersButton = findViewById(R.id.delivered_letters_button);
        LinearLayout notifiedLettersButton = findViewById(R.id.notified_letters_button);
        LinearLayout otherLettersButton = findViewById(R.id.other_letters_button);

        TextView allLettersNumber = findViewById(R.id.all_letters_number);
        TextView deliveredLettersNumber = findViewById(R.id.delivered_letters_number);
        TextView otherLettersNumer = findViewById(R.id.other_letters_number);
        TextView notifiedLettersNumber = findViewById(R.id.notified_letters_number);

        auth = FirebaseAuth.getInstance();

        letterViewModel = ViewModelProviders.of(this).get(LetterViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        letterViewModel.findNotYetDeliveredLetters().observe(MainMenuActivity.this, letters -> {
            allLettersNumber.setText(String.valueOf(letters.size()));
        });
        letterViewModel.findLettersWithStatus(getString(R.string.delivered)).observe(this, letters -> {
            deliveredLettersNumber.setText(String.valueOf(letters.size()));
        });
        letterViewModel.findLettersWithStatus(getString(R.string.notified)).observe(this, letters -> {
            notifiedLettersNumber.setText(String.valueOf(letters.size()));
        });
        letterViewModel.findLettersWithStatus(getString(R.string.other)).observe(this, letters -> {
            otherLettersNumer.setText(String.valueOf(letters.size()));
        });


        allLettersButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainMenuActivity.this, LettersListActivity.class);
            intent.putExtra(ACTION_NAME, ALL_CODE);
            startActivity(intent);
        });
        deliveredLettersButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainMenuActivity.this, LettersListActivity.class);
            intent.putExtra(ACTION_NAME, DELIVER_CODE);
            startActivity(intent);
        });
        otherLettersButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainMenuActivity.this, LettersListActivity.class);
            intent.putExtra(ACTION_NAME, OTHER_CODE);
            startActivity(intent);
        });
        notifiedLettersButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainMenuActivity.this, LettersListActivity.class);
            intent.putExtra(ACTION_NAME, NOTIFY_CODE);
            startActivity(intent);
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.download_drawer_menu:
                fetchAllLetters();
                break;
            case R.id.settle_drawer_menu:
                postAllLetters(letterViewModel);
                break;
            case R.id.logout_drawer_menu:
                auth.signOut();
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void postAllLetters(LetterViewModel letterViewModel) {
        LetterService letterService = RetrofitInstance.getInstance().create(LetterService.class);

        letterViewModel.findAll().observe(MainMenuActivity.this, letters -> {
            for(Letter l: letters) {
                if(l.getStatus() != null) {
                    Call<Letter> letterCall = letterService.updateLetter(l.getId(), l.getStatus(), l.getLetterInfo1(), l.getLetterInfo2());

                    letterCall.enqueue(new Callback<Letter>() {
                        @Override
                        public void onResponse(Call<Letter> call, Response<Letter> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.letters_updated, Toast.LENGTH_LONG).show();
                            } else
                                Toast.makeText(getApplicationContext(), R.string.letters_not_updated, Toast.LENGTH_LONG).show();
                        }
                        @Override
                        public void onFailure(Call<Letter> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), R.string.letters_updated, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private void fetchAllLetters() {
        LetterService letterService = RetrofitInstance.getInstance().create(LetterService.class);

        Call<List<Letter>> letterCall = letterService.getLetters();


        letterCall.enqueue(new Callback<List<Letter>>() {
            @Override
            public void onResponse(Call<List<Letter>> call, Response<List<Letter>> response) {
                if(response.body() != null) {
                    for (Letter l : response.body()) {
                        if(auth.getCurrentUser() != null)
                            if(l.getPostmanId().equals(auth.getCurrentUser().getEmail()))
                                letterViewModel.insert(l);
                    }
                }
                else
                    Toast.makeText(MainMenuActivity.this, R.string.no_letters,
                            Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Letter>> call, Throwable t) {
                Toast.makeText(MainMenuActivity.this, R.string.fetch_failed,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
