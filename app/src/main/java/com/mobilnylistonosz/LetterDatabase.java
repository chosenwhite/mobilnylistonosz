package com.mobilnylistonosz;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Letter.class}, version = 3, exportSchema = false)
public abstract class LetterDatabase extends RoomDatabase {

    public abstract LetterDao letterDao();

    private static volatile LetterDatabase INSTANCE;
    public static final int THREADS = 4;
    static final ExecutorService dbExecutor = Executors.newFixedThreadPool(THREADS);

    static LetterDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (Letter.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            LetterDatabase.class, "letters_db")
                            .addCallback(deleteAllLettersCallback)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback deleteAllLettersCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            dbExecutor.execute(()->{
                LetterDao dao = INSTANCE.letterDao();
                dao.deleteAll();
            });
        }
    };
}
