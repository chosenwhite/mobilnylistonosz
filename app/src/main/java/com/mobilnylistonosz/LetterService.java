package com.mobilnylistonosz;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LetterService {

    @GET("listy/all")
    Call<List<Letter>> getLetters();

    @POST("listy/update")
    @FormUrlEncoded
    Call<Letter>  updateLetter(@Field("id") String id,
                               @Field("status") String status,
                               @Field("letterInfo1") String letterInfo1,
                               @Field("letterInfo2") String letterInfo2);
}
