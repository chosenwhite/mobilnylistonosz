package com.mobilnylistonosz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NotifyActivity extends AppCompatActivity {

    private Button acceptButton;
    private Spinner notifyPlaceSpinner;
    private Spinner notifyPostSpinner;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_layout);

        acceptButton = findViewById(R.id.accept_button_notify);
        notifyPlaceSpinner = findViewById(R.id.notify_places_spinner);
        notifyPostSpinner = findViewById(R.id.notify_posts_spinner);
        recyclerView = findViewById(R.id.checked_letters_list);

        ArrayAdapter<String> placesSpinnerAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, getResources().getStringArray(R.array.notify_places));
        notifyPlaceSpinner.setAdapter(placesSpinnerAdapter);

        ArrayAdapter<String> postSpinnerAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, getResources().getStringArray(R.array.post_places));
        notifyPostSpinner.setAdapter(postSpinnerAdapter);

        LetterAdapter adapter = new LetterAdapter();
        adapter.setLetters((ArrayList<Letter>) getIntent().getSerializableExtra(LettersListActivity.CHECKED_LETTERS));

        ActionBar ab = getSupportActionBar();
        ab.setSubtitle(getResources().getString(R.string.checked_letters, adapter.getLetters().size()));

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        acceptButton.setOnClickListener(v -> {
            LetterViewModel letterViewModel = ViewModelProviders.of(this).get(LetterViewModel.class);
            for (Letter l : adapter.letters) {
                letterViewModel.updateLetter(getResources().getString(R.string.notified),
                        (String) notifyPlaceSpinner.getSelectedItem(),
                        (String) notifyPostSpinner.getSelectedItem(),
                        l.getId());
            }
            setResult(RESULT_OK);
            finish();
        });
    }
    private class LetterHolder extends RecyclerView.ViewHolder {

        private TextView reciverTextView;
        private TextView letterIdTextView;
        private CheckBox letterCheckBox;

        public LetterHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_layout, parent, false));

            letterIdTextView = itemView.findViewById(R.id.letter_id);
            reciverTextView = itemView.findViewById(R.id.letter_reciver);
            letterCheckBox = itemView.findViewById(R.id.letter_checkbox);

            letterCheckBox.setVisibility(View.GONE);

            itemView.setPadding(10, 5, 0, 5);
        }

        public void bind(Letter l) {
            letterIdTextView.setText(l.getId());
            reciverTextView.setText(l.getReceiverName() + " " + l.getReceiverAddress());
        }
    }
    private class LetterAdapter extends RecyclerView.Adapter<LetterHolder> {

        private List<Letter> letters;
        private ArrayList<Letter> checkedLetters = new ArrayList<>();

        @NonNull
        @Override
        public LetterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new LetterHolder(getLayoutInflater(), parent);
        }

        @Override
        public void onBindViewHolder(@NonNull LetterHolder holder, int position) {
            if(letters != null) {
                Letter letter = letters.get(position);

                holder.bind(letter);
            }
        }

        @Override
        public int getItemCount() {
            if(letters != null)
                return letters.size();
            else
                return 0;
        }

        public void setLetters(List<Letter> letters) {
            this.letters = letters;
            notifyDataSetChanged();
        }

        public List<Letter> getLetters() {
            return letters;
        }
    }
}
