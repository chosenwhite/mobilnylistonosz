package com.mobilnylistonosz;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class LetterViewModel extends AndroidViewModel {

    private LetterRepository letterRepository;

    public LetterViewModel(@NonNull Application application) {
        super(application);
        letterRepository = new LetterRepository(application);
    }

    public LiveData<List<Letter>> findAll() {
        return letterRepository.getAllLetters();
    }

    public void insert(Letter book) {
        letterRepository.insert(book);
    }

    public void update(Letter book) {
        letterRepository.update(book);
    }

    public void delete(Letter book) {
        letterRepository.delete(book);
    }

    public LiveData<List<Letter>> findNotYetDeliveredLetters() {
        return letterRepository.findNotYetDeliveredLetters();
    }

    public LiveData<List<Letter>> findLettersWithStatus(String newStatus) {
        return letterRepository.findLettersWithStatus(newStatus);
    }

    void updateLetter(String newStatus, String newLetterInfo1, String newLetterInfo2, String newIdListu) {
        letterRepository.updateLetter(newStatus, newLetterInfo1, newLetterInfo2, newIdListu);
    }
}
