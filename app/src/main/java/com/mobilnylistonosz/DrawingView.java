package com.mobilnylistonosz;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

public class DrawingView extends LinearLayout implements View.OnTouchListener {

    private Paint paint;
    private Path path;

    public DrawingView(Context context) {
        super(context);
        initView();
    }

    public DrawingView(Context context, @NonNull AttributeSet attrSet) {
        super(context, attrSet);
        initView();
    }

    public DrawingView(Context context, @NonNull AttributeSet attrSet, int defStyleAttr) {
        super(context, attrSet, defStyleAttr);
        initView();
    }

    public void initView() {
        path = new Path();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);

        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawPath(path, paint);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float x, y;
        x = event.getX();
        y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(x, y);
                path.addCircle(x, y, 1, Path.Direction.CCW);
                break;
            case MotionEvent.ACTION_MOVE:
                path.lineTo(x, y);
                break;
        }
        invalidate();

        return true;
    }
}