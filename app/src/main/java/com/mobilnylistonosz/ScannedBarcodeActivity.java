package com.mobilnylistonosz;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.snackbar.Snackbar;

import info.androidhive.barcode.BarcodeReader;

import java.util.List;

public class ScannedBarcodeActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    public static final String BARCODE_DATA = "barcode_text";
    String barcodeData = "";

    private BarcodeReader barcodeReader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_scan_layout);

        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    @Override
    public void onScanned(Barcode barcode) {

        barcodeReader.playBeep();

        barcodeData = barcode.rawValue;

        Intent reply = new Intent();
        reply.putExtra(BARCODE_DATA, barcodeData);
        setResult(RESULT_OK ,reply);
        finish();
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        finish();
    }

    @Override
    public void onCameraPermissionDenied() {
        Snackbar.make(findViewById(R.id.relative_layout), R.string.no_camera_permission, Snackbar.LENGTH_LONG).show();
        finish();
    }
}