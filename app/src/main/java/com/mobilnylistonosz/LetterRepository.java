package com.mobilnylistonosz;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class LetterRepository {

    private LetterDao letterDao;

    public LetterRepository(Application application) {
        LetterDatabase database = LetterDatabase.getInstance(application.getApplicationContext());
        letterDao = database.letterDao();
    }

    public LiveData<List<Letter>> getAllLetters() {
        return letterDao.findAllLetters();
    }

    public void insert(Letter letter) {
        LetterDatabase.dbExecutor.execute(() -> {
            letterDao.insert(letter);
        });
    }

    public void update(Letter letter) {
        LetterDatabase.dbExecutor.execute(() -> {
            letterDao.update(letter);
        });
    }

    public void delete(Letter letter) {
        LetterDatabase.dbExecutor.execute(() -> {
            letterDao.delete(letter);
        });
    }

    public LiveData<List<Letter>> getLettersWithId(String id) {
        return letterDao.findLettersWithId(id);
    }

    public  LiveData<List<Letter>> findNotYetDeliveredLetters() {
        return letterDao.findNotYetDeliveredLetters();
    }

    LiveData<List<Letter>> findLettersWithStatus(String newStatus) {
        return letterDao.findLettersWithStatus(newStatus);
    }

    void updateLetter(String newStatus, String newLetterInfo1, String newLetterInfo2, String newIdListu) {
        LetterDatabase.dbExecutor.execute(() -> {
            letterDao.updateLetter(newStatus, newLetterInfo1, newLetterInfo2, newIdListu);
        });
    }
}
