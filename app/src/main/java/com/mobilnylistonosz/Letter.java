package com.mobilnylistonosz;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "letters")
public class Letter implements Serializable {

    @NonNull
    @PrimaryKey
    @SerializedName("idListu")
    private String id;
    @SerializedName("emailListonosza")
    private String postmanId;
    @SerializedName("odbiorcaImie")
    private String receiverName;
    @SerializedName("odbiorcaAdres")
    private String receiverAddress;
    @SerializedName("adresatImie")
    private String senderName;
    @SerializedName("adresatAdres")
    private String senderAddress;

    private String letterInfo1;
    private String letterInfo2;
    private String status;

    public Letter() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLetterInfo1() {
        return letterInfo1;
    }

    public void setLetterInfo1(String letterInfo1) {
        this.letterInfo1 = letterInfo1;
    }

    public String getLetterInfo2() {
        return letterInfo2;
    }

    public void setLetterInfo2(String letterInfo2) {
        this.letterInfo2 = letterInfo2;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getPostmanId() {
        return postmanId;
    }

    public void setPostmanId(String postmanId) {
        this.postmanId = postmanId;
    }
}
