package com.mobilnylistonosz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText loginEditText;
    private EditText passwordEditText;
    private TextView authInfoTextView;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        loginButton = findViewById(R.id.login_button);
        loginEditText = findViewById(R.id.login_input);
        passwordEditText = findViewById(R.id.password_input);
        authInfoTextView = findViewById(R.id.auth_info);

        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        auth = FirebaseAuth.getInstance();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(loginEditText.getText()) || TextUtils.isEmpty(passwordEditText.getText())) {
                    authFailed();
                }
                else
                    login();
            }
        });
    }
    private void login() {
        auth.signInWithEmailAndPassword(loginEditText.getText().toString(), passwordEditText.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                            startActivity(intent);
                        }else {
                            authFailed();
                        }
                    }
                });
    }

    private void authFailed() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(authInfoTextView, "translationX", 15, -15, 10, -10, 5, -5 ,0);
        animator.setDuration(200);
        animator.start();

        if(TextUtils.isEmpty(loginEditText.getText())) {
            authInfoTextView.setText(getResources().getString(R.string.empty_login_password));
            loginEditText.setHintTextColor(getResources().getColor(R.color.wrong_input_hint_color));
        }
        else
            authInfoTextView.setText(getResources().getString(R.string.wrong_password));

        passwordEditText.setHintTextColor(getResources().getColor(R.color.wrong_input_hint_color));
        passwordEditText.setText("");
    }
}
