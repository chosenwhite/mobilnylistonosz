package com.mobilnylistonosz;

import java.util.List;

public class LetterContainer {

    private List<Letter> lettersList;

    public List<Letter> getLettersList() {
        return lettersList;
    }

    public void setLettersList(List<Letter> lettersList) {
        this.lettersList = lettersList;
    }
}
