package com.mobilnylistonosz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class SignatureActivity extends AppCompatActivity {

    private Button acceptButton;
    private Button clearButton;
    private DrawingView drawingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature_layout);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        drawingView = findViewById(R.id.drawing_field);
        acceptButton = findViewById(R.id.accept_button_delivery);
        clearButton = findViewById(R.id.clear_button);

        clearButton.setOnClickListener(v -> {
            ActivityCompat.recreate(SignatureActivity.this);
        });

        acceptButton.setOnClickListener(v -> {
            Bitmap bitmap = Bitmap.createBitmap(drawingView.getWidth(), drawingView.getHeight(), Bitmap.Config.ARGB_8888);
            String extr = Environment.getExternalStorageDirectory().toString();
            File myPath = new File(extr, UUID.randomUUID().toString() +".jpg");
            FileOutputStream fos = null;

            Canvas c = new Canvas(bitmap);
            drawingView.draw(c);

            try {
                myPath.createNewFile();
                fos = new FileOutputStream(myPath);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            setResult(RESULT_OK);
            Intent reply =new Intent();
            reply.putExtra(DeliverActivity.SIGN_FILE, myPath.getName());
            finish();
        });
    }
}