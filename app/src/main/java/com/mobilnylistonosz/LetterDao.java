package com.mobilnylistonosz;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.UUID;

@Dao
public interface LetterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Letter letter);

    @Update
    void update(Letter letter);

    @Delete
    void delete(Letter letter);

    @Query("Update letters set status=:newStatus, letterInfo1=:newLetterInfo1, letterInfo2=:newLetterInfo2 where id=:newIdListu")
    void updateLetter(String newStatus, String newLetterInfo1, String newLetterInfo2, String newIdListu);

    @Query("Delete from letters")
    void deleteAll();

    @Query("Select * from letters")
    LiveData<List<Letter>> findAllLetters();

    @Query("Select * from letters where id=:letterId")
    LiveData<List<Letter>> findLettersWithId(String letterId);

    @Query("Select * from letters where status is NULL")
    LiveData<List<Letter>> findNotYetDeliveredLetters();

    @Query("Select * from letters where status=:newStatus")
    LiveData<List<Letter>> findLettersWithStatus(String newStatus);
}
