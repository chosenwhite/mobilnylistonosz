package com.mobilnylistonosz;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance  {

    private static Retrofit instance;
    public static final String LETTERS_URL = "http://tescikus.herokuapp.com/";


    public static Retrofit getInstance() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(new BasicAuthInterceptor("admin","Michaljestfajny")).build();

        if(instance == null) {
            instance = new Retrofit.Builder()
                    .baseUrl(LETTERS_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return instance;
    }
}
