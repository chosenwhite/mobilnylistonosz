package com.mobilnylistonosz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class LettersListActivity extends AppCompatActivity {

    public static final int BARCODE_ACTIVITY_REQUEST_CODE = 1;
    public static final int DELIVER_LETTERS_REQUEST_CODE = 2;
    public static final String CHECKED_LETTERS = "letters";

    private LetterViewModel letterViewModel;
    private Button actionButton;
    private LetterAdapter adapter;
    private String barcodeData;
    private SearchView search;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.letters_list_layout);

        RecyclerView recyclerView = findViewById(R.id.recycler_view2);

        actionButton = findViewById(R.id.action_button);
        actionButton.setEnabled(false);
        switch (getIntent().getIntExtra(MainMenuActivity.ACTION_NAME, 0)) {
            case 1:
                actionButton.setVisibility(View.GONE);
                break;
            case 2:
                actionButton.setText(getResources().getString(R.string.deliver_letters));
                break;
            case 3:
                actionButton.setText(getResources().getString(R.string.add_to_other));
                break;
            case 4:
                actionButton.setText(getResources().getString(R.string.notify_letters));
                break;
        }
        actionButton.setOnClickListener(v -> {
            switch (getIntent().getIntExtra(MainMenuActivity.ACTION_NAME, 0)) {
                case 2:
                    Intent intent_delivery = new Intent(LettersListActivity.this, DeliverActivity.class);
                    intent_delivery.putExtra(CHECKED_LETTERS, adapter.getCheckedLetters());
                    startActivityForResult(intent_delivery, DELIVER_LETTERS_REQUEST_CODE);
                    break;
                case 3:
                    Intent intent_other = new Intent(LettersListActivity.this, OtherActivity.class);
                    intent_other.putExtra(CHECKED_LETTERS, adapter.getCheckedLetters());
                    startActivityForResult(intent_other, DELIVER_LETTERS_REQUEST_CODE);
                    break;
                case 4:
                    Intent intent_notify = new Intent(LettersListActivity.this, NotifyActivity.class);
                    intent_notify.putExtra(CHECKED_LETTERS, adapter.getCheckedLetters());
                    startActivityForResult(intent_notify, DELIVER_LETTERS_REQUEST_CODE);
                    break;
            }
        });

        adapter = new LetterAdapter();
        recyclerView.setAdapter(adapter);
        letterViewModel = ViewModelProviders.of(this).get(LetterViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.list_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        letterViewModel.findNotYetDeliveredLetters().observe(LettersListActivity.this, letters -> adapter.setLetters(letters));
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.letters_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        search = searchView;

        List<Letter> allLetters = adapter.getLetters();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                List<Letter> searchLetters = findLetter(query, allLetters);
                searchLetters.removeAll(adapter.getCheckedLetters());
                searchLetters.addAll(adapter.getCheckedLetters());

                adapter.setLetters(searchLetters);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Letter> searchLetters = findLetter(newText, allLetters);
                searchLetters.removeAll(adapter.getCheckedLetters());
                searchLetters.addAll(adapter.getCheckedLetters());

                if(newText.length() != 0)
                    adapter.setLetters(searchLetters);
                else
                    adapter.setLetters(allLetters);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.qr_search) {
            startActivityForResult(new Intent(this, ScannedBarcodeActivity.class), BARCODE_ACTIVITY_REQUEST_CODE);
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == BARCODE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            barcodeData = data.getStringExtra(ScannedBarcodeActivity.BARCODE_DATA);

            if(search != null)
                search.post(() -> {
                    search.setQuery(barcodeData, true);
                    search.setIconified(false);
                    barcodeData = null;
                });
        }

        else if(requestCode == DELIVER_LETTERS_REQUEST_CODE && resultCode == RESULT_OK) {
            adapter.setCheckedLetters(new ArrayList<>());
            actionButton.setEnabled(false);
        }
    }

    public List<Letter> findLetter(String query, List<Letter> allLetters) {
        List<Letter> searchLetters = new ArrayList<>();
        if(allLetters != null)
            for (Letter letter : allLetters) {
                if(letter.getId().contains(query) || letter.getReceiverName().contains(query) || letter.getReceiverAddress().contains(query))
                    searchLetters.add(letter);
            }
        return searchLetters;
    }

    private class LetterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView reciverTextView;
        private TextView letterIdTextView;
        private CheckBox letterCheckBox;

        public LetterHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_layout, parent, false));

            letterIdTextView = itemView.findViewById(R.id.letter_id);
            reciverTextView = itemView.findViewById(R.id.letter_reciver);
            letterCheckBox = itemView.findViewById(R.id.letter_checkbox);

            if(getIntent().getIntExtra(MainMenuActivity.ACTION_NAME, 0) == 1) {
                letterCheckBox.setVisibility(View.GONE);
                itemView.setPadding(10, 5, 0, 5);
            }
            else
                itemView.setOnClickListener(this);
        }

        public void bind(Letter l) {
            letterIdTextView.setText(l.getId());
            reciverTextView.setText(l.getReceiverName() + " " + l.getReceiverAddress());
            if(adapter.getCheckedLetters().contains(l))
                letterCheckBox.setChecked(true);
            else
                letterCheckBox.setChecked(false);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Letter l = adapter.getLetters().get(position);
            if(adapter.getCheckedLetters().contains(l)) {
                adapter.getCheckedLetters().remove(l);
                letterCheckBox.setChecked(false);
            }
            else {
                adapter.getCheckedLetters().add(l);
                letterCheckBox.setChecked(true);
            }
        }
    }
    private class LetterAdapter extends RecyclerView.Adapter<LetterHolder> {

        private List<Letter> letters;
        private ArrayList<Letter> checkedLetters = new ArrayList<>();

        @NonNull
        @Override
        public LetterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new LetterHolder(getLayoutInflater(), parent);
        }

        @Override
        public void onBindViewHolder(@NonNull LetterHolder holder, int position) {
            if(letters != null) {
                Letter letter = letters.get(position);

                holder.bind(letter);

                holder.letterCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(checkedLetters.isEmpty())
                            actionButton.setEnabled(false);
                        else
                            actionButton.setEnabled(true);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            if(letters != null)
                return letters.size();
            else
                return 0;
        }

        public void setLetters(List<Letter> letters) {
            this.letters = letters;
            notifyDataSetChanged();
        }

        public List<Letter> getLetters() {
            return letters;
        }

        public ArrayList<Letter> getCheckedLetters() {
            return checkedLetters;
        }

        public void setCheckedLetters(ArrayList<Letter> checkedLetters) {
            this.checkedLetters = checkedLetters;
        }
    }
}
